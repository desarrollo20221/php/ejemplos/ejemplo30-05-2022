<?php
    session_start();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <div>
            <?php
                // comprobar si existe la variable de sesion nombre
                if(isset($_SESSION["nombre"])){
                    // si existe muestro un mensaje como el siguiente
                    // el ultimo nombre escrito es aqui_coloco_el_nombre
                    echo "El ultimo nombre escrito es {$_SESSION["nombre"]}";
                } else {                           
                    // si no existe la variable de sesion
                    // muestro el mensaje
                    // No has cargado el formulario
                    echo "No has cargado el formulario";
                }
            ?>
        </div>

    </body>
</html>
